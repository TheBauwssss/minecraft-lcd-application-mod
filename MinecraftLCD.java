package ricksoft.minecraft.lcd;

import net.minecraft.client.Minecraft;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid=MinecraftLCD.MODID, name="Minecraft LCD Application", version=MinecraftLCD.VERSION)
public class MinecraftLCD
{
  LCDConnectionListener theLCDConnection;
  
  public static Boolean debugMode = true;
  
  public static final String MODID = "ricksoft_mc-lcd-app";
  public static final String VERSION = "0.3";
  
  @EventHandler
  public void init(FMLInitializationEvent event)
  {
	  try
	    {
	      System.out.println("Starting LCD support. Binding to localhost:" + LCDConnectionListener.port);
	      try {
	    	this.theLCDConnection = new LCDConnectionListener(Minecraft.getMinecraft());
	        System.out.println("LCD support started. Enjoy Minecraft with your LCD enabled keyboard :D.");
	      } catch (Exception e) {
	        System.out.println("Unable to start LCD support. A connection may already have been initialized.");
	      }
	    }
	    catch (Exception var11)
	    {
	    }
  }

}