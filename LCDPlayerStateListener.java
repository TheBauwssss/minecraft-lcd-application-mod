package ricksoft.minecraft.lcd;

import java.io.PrintWriter;

import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;

public class LCDPlayerStateListener extends Thread{
	
	Minecraft MC;
	LCDConnectionListener connListener;
	
	public LCDPlayerStateListener(Minecraft theMC, LCDConnectionListener connList){
		MC = theMC;
		connListener = connList;
	}
	
	int modVersion = 2;
	int x = -1;
	int y = -1;
	int z = -1;
	int lastHealth = -1;
	int lastMaxHealth = -1;
	int lastHunger = -1; //Max is ALWAYS 20
	int lastArmorLevel = -1;
	int lastIsDead = -1;
	int lastScore = -1;
	long lastWorldTime = -1;
	int inGame = -1;
	float lastXP = -1;
	int lastLevel = -1;
	int isPoisoned = -1;
	int lastlevel = -1;
	
	public void Reset(){
		lastHealth = -1;
		lastMaxHealth = -1;
		lastHunger = -1; //Max is ALWAYS 20
		lastArmorLevel = -1;
		lastIsDead = -1;
		lastScore = -1;
		lastWorldTime = -1;
		inGame = -1;
		lastXP = -1;
		lastLevel = -1;
		isPoisoned = -1;
		lastLevel = -1;
		
		x = -1;
		y = -1;
		z = -1;
		
		connListener.Update("Reset()"); 
		
		connListener.Update("modVersion=" + modVersion);
		
	}
	
	Boolean debug = MinecraftLCD.debugMode;
	
	long lastPosUpdate = -1;
	
	public void run(){

		while(true)
		{
			
			if (inGame == 1)
			{
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
				
				}
			}
			else
			{
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					
				}
			}
			
			if (connListener.sock == null | connListener.out == null)
			{
				try {
					Thread.sleep(250);
					
					
				} catch (InterruptedException e) {
					
				}
				
			}
			else if(MC.thePlayer != null | MC.theWorld != null)
			{
				try {
				
					if (System.currentTimeMillis() - lastPosUpdate >= 250 | lastPosUpdate == -1) {
						lastPosUpdate = System.currentTimeMillis();
						//Check the player cords
						int newx, newy, newz;
						newx = (int)Math.floor(MC.thePlayer.posX);
						newy = (int)Math.floor(MC.thePlayer.posY);
						newz = (int)Math.floor(MC.thePlayer.posZ);
						if (newx != x | newy != y | newz != z) {
							connListener.Update("XYZ=" + newx + "," + newy + "," + newz);
							x = newx;
							y = newy;
							z = newz;
							if (debug) {
								System.out.println("XYZ=" + newx + "," + newy + "," + newz);
							}
						}
					}
					//Check is player is poisoned
					boolean currentPoisonState = MC.thePlayer.isPotionActive(Potion.poison);
					int currentPoisonStateInt = 0;
					
					if (currentPoisonState) {currentPoisonStateInt = 1;} else {currentPoisonStateInt = 0;}
					
					if (currentPoisonStateInt != isPoisoned)
					{
						connListener.Update("IsPlayerPoisoned=" + currentPoisonStateInt); 
						isPoisoned = currentPoisonStateInt; 
						if(debug){System.out.println("IsPlayerPoisoned=" + currentPoisonStateInt);}
					}
					
					
					//Check current player health
					int currentHealth = (int) MC.thePlayer.getHealth(); //getHealth()
					if (currentHealth != lastHealth)
					{
						connListener.Update("CurrentHealth=" + currentHealth); 
						lastHealth = currentHealth; 
						if(debug){System.out.println("CurrentHealth=" + currentHealth);}
					}
					
					//Check maximum player health
					int currentMaxHealth = (int) MC.thePlayer.getMaxHealth(); //getMaximumHealth
					if (currentMaxHealth != lastMaxHealth)
					{
						connListener.Update("MaximumHealth=" + currentMaxHealth); 
						lastMaxHealth = currentHealth; 
						if(debug){System.out.println("MaximumHealth=" + currentMaxHealth);}
					}
					
					//Check time
					long currentWorldTime = Math.round(MC.theWorld.getWorldTime() / 100) * 100;
					if (currentWorldTime != lastWorldTime)
					{
						connListener.Update("CurrentWorldTime=" + currentWorldTime); 
						lastWorldTime = currentWorldTime; 
						if(debug){System.out.println("CurrentWorldTime=" + currentWorldTime);}
					}
					
					//Check current hunger
					int currentHunger = MC.thePlayer.getFoodStats().getFoodLevel();
					if (currentHunger != lastHunger)
					{
						connListener.Update("CurrentHunger=" + currentHunger); 
						lastHunger = currentHunger; 
						if(debug){System.out.println("CurrentHunger=" + currentHunger);}
					}
						
					
					//Check score
					int currentScore = MC.thePlayer.getScore();
					if (currentScore != lastScore)
					{
						connListener.Update("CurrentScore=" + currentScore); 
						lastScore = currentScore; 
						if(debug){System.out.println("CurrentScore=" + currentScore);}
					}
					
					//Check is player dead
					boolean currentIsDead = MC.thePlayer.isDead;
					int currentIsDeadInt = 0;
					
					if (currentIsDead) {currentIsDeadInt = 1;} else {currentIsDeadInt = 0;}
					
					if (currentIsDeadInt != lastIsDead)
					{
						connListener.Update("IsPlayerDead=" + currentIsDeadInt); 
						lastIsDead = currentIsDeadInt; 
						if(debug){System.out.println("IsPlayerDead=" + currentIsDeadInt);}
					}
					
					//Check XP
					
					float currentXP = MC.thePlayer.experience;
					if (currentXP != lastXP)
					{
						connListener.Update("CurrentXP=" + currentXP); 
						lastXP = currentXP; 
						if(debug){System.out.println("CurrentXP=" + currentXP);}
					}
					
					
					//Check last level
					int currentLevel = MC.thePlayer.experienceLevel;
					if (currentLevel != lastLevel)
					{
						connListener.Update("CurrentLevel=" + currentLevel); 
						lastLevel = currentLevel; 
						if(debug){System.out.println("CurrentLevel=" + currentLevel);}
					}
					
					
					//Check armor level
					int currentArmorLevel = MC.thePlayer.getTotalArmorValue();
					if (currentArmorLevel != lastArmorLevel)
					{
						connListener.Update("Armor=" + currentArmorLevel); 
						lastArmorLevel = currentArmorLevel; 
						if(debug){System.out.println("Armor=" + currentArmorLevel);}
					}

					
					//Check game status
					if (inGame != 1){
						inGame = 1;
						
						connListener.Update("InGame=" + inGame);
						
						if(debug){System.out.println("InGame=" + inGame);}
						
					}
					
					
				} catch (Exception e) {
					if (inGame != 0){
						inGame = 0;
						Reset();
						connListener.Update("InGame=" + inGame);
						
						if(debug){System.out.println("InGame=" + inGame);}
						
						try {
							Thread.sleep(250);
						} catch (InterruptedException e1) {
							
						}
						
					}
				}
				
				
			}
			else
			{
				try {
					
					if (inGame != 0){
						inGame = 0;
						
						connListener.Update("InGame=" + inGame);
						
						if(debug){System.out.println("InGame=" + inGame);}
						
					}
					Thread.sleep(250);
				} catch (InterruptedException e) {
					
				}
			}
			
			
		}
	
	}
	
}
